---
- hosts: all
  tasks:
    - name: tune TCP settings for Azure
      ansible.builtin.copy:
        dest: /etc/sysctl.d/azure_tcp_settings.conf
        content: |
          net.ipv4.tcp_keepalive_time = 120
          net.ipv4.tcp_keepalive_intvl = 30
          net.ipv4.tcp_keepalive_probes = 8
      become: true

    - name: remove leap repositories
      community.general.zypper_repository:
        name: "{{ item }}"
        state: absent
      loop:
        - repo-backports-debug-update
        - repo-backports-update
        - repo-debug
        - repo-debug-update
        - repo-non-oss
        - repo-oss
        - repo-sle-debug-update
        - repo-sle-update
        - repo-source
        - repo-update
        - repo-update-non-oss
      become: true
      register: repo_removed

    - name: add tumbleweed repositores
      community.general.zypper_repository:
        name: "{{ item.name }}"
        repo: "{{ item.repo }}"
        auto_import_keys: true
      loop:
        - { name: tumbleweed-oss, repo: "http://download.opensuse.org/tumbleweed/repo/oss" }
        - { name: tumbleweed-non-oss, repo: "http://download.opensuse.org/tumbleweed/repo/non-oss" }
        - { name: tumbleweed-update, repo: "http://download.opensuse.org/update/tumbleweed/" }
      become: true
      register: repo_added

    - name: Clear zypper cache if repositories were changed
      ansible.builtin.command: zypper cc -a
      become: true
      when: repo_added.changed or repo_removed.changed

    - name: update all packages
      community.general.zypper:
        name: '*'
        allow_vendor_change: "{{ repo_added.changed or repo_removed.changed | bool }}"
        state: dist-upgrade
      ignore_errors: "{{ repo_added.changed or repo_removed.changed | bool }}"
      become: true

    - name: Reboot machine if repositories were changed
      ansible.builtin.reboot:
      when: repo_added.changed or repo_removed.changed
      become: true

    - name: install base dev packages
      community.general.zypper:
        name:
          - emacs-nox
          - git-core
          - highlight
          - jq
          - stow
          - tree
          - tmux
          - zsh
      become: true

    - name: clone dotfiles repo
      ansible.builtin.git:
        repo: 'https://gitlab.com/jarrett/dotfiles.git'
        dest: "{{ ansible_env.HOME }}/dotfiles"

    - name: clone emacs repo
      ansible.builtin.git:
        repo: 'https://gitlab.com/jarrett/emacs-config.git'
        dest: "{{ ansible_env.HOME }}/.emacs.d"

    - name: stow zshrc
      ansible.builtin.command: /usr/bin/stow zsh
      args:
        chdir: "{{ ansible_env.HOME }}/dotfiles"
        creates: "{{ ansible_env.HOME }}/.zshrc"

    - name: create .zshrc.d
      ansible.builtin.file:
        path: "{{ ansible_env.HOME }}/.zshrc.d"
        state: directory

    - name: stow tmux config
      ansible.builtin.command: /usr/bin/stow tmux
      args:
        chdir: "{{ ansible_env.HOME }}/dotfiles"
        creates: "{{ ansible_env.HOME }}/.tmux.conf"

    - name: stow zypper dup alias
      ansible.builtin.command: /usr/bin/stow zypper_dup
      args:
        chdir: "{{ ansible_env.HOME }}/dotfiles"
        creates: "{{ ansible_env.HOME }}/.zshrc.d/zypper_dup.zsh"

    - name: remove default .emacs file
      ansible.builtin.file:
        path: "{{ ansible_env.HOME }}/.emacs"
        state: absent

    - name: enable & start emacs daemon
      ansible.builtin.systemd:
        name: emacs
        state: started
        enabled: true
        scope: user
      async: 600
      poll: 0
      register: emacs_daemon

    - name: add zsh module repositores
      community.general.zypper_repository:
        repo: "https://download.opensuse.org/repositories/shells:zsh-users:zsh-{{ item }}/openSUSE_Tumbleweed/shells:zsh-users:zsh-{{ item }}.repo"
        auto_import_keys: true
      loop:
        - autosuggestions
        - syntax-highlighting
        - completions
      become: true

    - name: install zsh modules
      community.general.zypper:
        name:
          - zsh-autosuggestions
          - zsh-syntax-highlighting
          - zsh-completions
      become: true

    - name: add zsh modules to .zshrc.d
      ansible.builtin.copy:
        dest: "{{ ansible_env.HOME }}/.zshrc.d/zsh-modules.zsh"
        content: |
          source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
          source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

    - name: switch user default shell to zsh
      ansible.builtin.user:
        name: "{{ ansible_user_id }}"
        shell: "/usr/bin/zsh"
      become: true

    - name: Setup Tailscale
      block:
        - name: Add Tailscale repo
          community.general.zypper_repository:
            repo: "https://pkgs.tailscale.com/stable/opensuse/tumbleweed/tailscale.repo"

        - name: Install Tailscale
          community.general.zypper:
            name: tailscale

        - name: Enable Tailscale service
          ansible.builtin.systemd:
            name: tailscaled
            state: started
            enabled: true

        - name: Get tailscale status
          ansible.builtin.command: tailscale status
          register: ts_status
          failed_when: false
          changed_when: false

        - name: Join Talinet
          ansible.builtin.command: "tailscale up --auth-key {{ ts_auth_key }}"
          when: ts_status.rc == 1
      become: true
      when: (ts_auth_key is defined) and (ts_auth_key|length > 0)

    - name: wait for emacs daemon to start
      ansible.builtin.async_status:
        jid: '{{ emacs_daemon.ansible_job_id }}'
      register: job_result
      until: job_result.finished
      retries: 60
      delay: 10
