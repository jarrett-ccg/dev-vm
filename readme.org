Deploys an openSUSE Leap VM on Azure and converts it to Tumbleweed. Then it installs things to help me feel at home.

Use it like this:

#+begin_src hcl
locals {
  ssh_key_path = "~/.ssh/id_rsa"
  ts_auth_key = "tskey-xxx"      # set to empty string to disable tailscale
}

module "dev_vm" {
  source = "./tf-module"

  id              = "dev"
  username        = "user"
  rsa_pubkey_path = "${local.ssh_key_path}.pub"
  location        = "East US 2"
  image = {
    publisher = "SUSE"
    offer     = "opensuse-leap-15-4"
    sku       = "gen2"
    version   = "latest"
  }
}

resource "null_resource" "ansible" {
  connection {
    type        = "ssh"
    user        = module.dev_vm.username
    private_key = file(local.ssh_key_path)
    host        = module.dev_vm.fqdn
  }

  provisioner "remote-exec" {
    script = "scripts/wait_for_instance.sh"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ${module.dev_vm.username} -i ${module.dev_vm.fqdn}, --key-file ${local.ssh_key_path} -e ts_auth_key=${local.ts_auth_key} --ssh-common-args='-o UserKnownHostsFile=/dev/null' ansible/main.yml"

    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
}

output "fqdn" {
  value = module.dev_vm.fqdn
}

output "username" {
  value = module.dev_vm.username
}
#+end_src
